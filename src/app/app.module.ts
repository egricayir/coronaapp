import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SpinnerComponent } from './spinner/spinner/spinner.component';
import { SpinnerOverlayComponent } from './spinner/spinner-overlay/spinner-overlay.component';
import { SpinnerOverlayWrapperComponent } from './spinner/spinner-overlay-wrapper/spinner-overlay-wrapper.component';
import { LoaderComponent } from './spinner/loader/loader.component';
import { DataService } from './service/data.service';
import { LoaderService } from './service/loader.service';
import { LoaderInterceptorService } from './service/loader.interceptor';
import { ChartsModule } from 'ng2-charts';
import {DatePipe} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    NotFoundComponent,
    FooterComponent,
    SpinnerComponent,
    SpinnerOverlayComponent,
    SpinnerOverlayWrapperComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ChartsModule
  ],
  providers: [
    DataService,
    LoaderService,
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
