import { Component, OnInit } from '@angular/core';
import { DataService } from '../../service/data.service';
import { CombinedResponse } from '../../models/combinedResponse';
import { StateResponse } from '../../models/stateResponse';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  stateObjects;
  stateObjectKeys;
  currentStateCode: string;
  currentStateName: string;
  currentStateResponse: StateResponse = new StateResponse("", 0, 0, 0);
  timeInterval: string = "";
  currentStateResponseWithDays: Array<CombinedResponse> = [];

  chartIstSichtbar: boolean;
  chartTyp: ChartType = "bar";
  chartTypArray: Array<string> = ["bar", "line", "radar", "polarArea", "pie", "doughnut"];
  chartDataset: ChartDataSets[] = [];
  chartColors: Array<any> = [];
  chartLabels: Label[] = [];

  chartOptions: ChartOptions = {
    responsive: true,
    legend: {
      labels: {
        fontColor: 'black',
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          fontColor: 'black',
          beginAtZero: true
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: 'black',
        }
      }]
    }
  };

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getAllStates().subscribe(
      response => {
        this.stateObjects = response["data"];
        this.stateObjectKeys = Object.keys(response["data"]);
      }
    );
  }

  getStateInfo(stateId: string){
    this.timeInterval = "today";
    this.chartIstSichtbar = false;
    this.chartDataset = [];
    this.chartLabels = [];

    if(stateId == 'Germany'){
      this.dataService.getCurrentGermany().subscribe(
        result => {
          this.currentStateResponse = new StateResponse(
            "Germany",
            result["delta"]["cases"],
            result["delta"]["deaths"],
            result["delta"]["recovered"]
          );
        }
      );
    }else {
      this.dataService.getCurrentFromState(stateId).subscribe(
        result => {
          this.currentStateResponse = new StateResponse(
            result["data"][stateId]["name"],
            result["data"][stateId]["delta"]["cases"],
            result["data"][stateId]["delta"]["deaths"],
            result["data"][stateId]["delta"]["recovered"]
          );
        }
      );
    }
  }

  getStateInfoWithDays(stateId: string, days: number){
    this.timeInterval = days + "days";
    this.currentStateResponseWithDays = [];

    if(stateId == 'Germany'){
      this.dataService.getCombinedGermany(stateId, days).subscribe(
        result => {
          this.mapCombinedState(result);
        }
      );
    }else {
      this.dataService.getCombinedState(stateId, days).subscribe(
        result => {
          this.mapCombinedState(result);
        }
      );
    }
    this.chartIstSichtbar = true;
  }

  onSelectState(stateId: string, stateName: string){
    this.currentStateCode = stateId;
    this.currentStateName = stateName;
    this.timeInterval = "";
    this.getStateInfo(stateId);
  }

  mapCombinedState(tempResult){
    let totalCases = 0;
    let totalDeaths = 0;
    let totalRecovered = 0;
    let totalCombinedResponse = new CombinedResponse();

    this.chartDataset = [];
    this.chartLabels = [];
    let casesDataSets: Array<number> = [];
    let deathsDataSets: Array<number> = [];
    let recoveredDataSets: Array<number> = [];

    tempResult.forEach((value, index) => {
      value.forEach((value2, index2) => {
        if(index == 0){
          this.currentStateResponseWithDays[index2] = new CombinedResponse();
          this.currentStateResponseWithDays[index2].date = value2["date"];
          this.currentStateResponseWithDays[index2].cases = value2["cases"];
          totalCases += value2["cases"];
          var options = { day: '2-digit', month: '2-digit', year: 'numeric' };
          this.chartLabels.push(new Date(value2["date"]).toLocaleDateString("de-DE", options));
          casesDataSets.push(value2["cases"]);
        }

        if(index == 1){
          this.currentStateResponseWithDays[index2].deaths = value2["deaths"];
          totalDeaths += value2["deaths"];
          deathsDataSets.push(value2["deaths"]);
        }else if(index == 2){
          this.currentStateResponseWithDays[index2].recovered = value2["recovered"];
          totalRecovered += value2["recovered"];
          recoveredDataSets.push(value2["recovered"]);
        }
      });
    });

    totalCombinedResponse.date = "Total";
    totalCombinedResponse.cases = totalCases;
    totalCombinedResponse.deaths = totalDeaths;
    totalCombinedResponse.recovered = totalRecovered;
    this.currentStateResponseWithDays.push(totalCombinedResponse);

    this.pushDataIntoDatachart(casesDataSets, "Cases");
    this.assignColorToChart(this.chartColors, "blue");
    this.pushDataIntoDatachart(deathsDataSets, "Deaths");
    this.assignColorToChart(this.chartColors, "red");
    this.pushDataIntoDatachart(recoveredDataSets, "Recovered");
    this.assignColorToChart(this.chartColors, "green");
  }

  pushDataIntoDatachart(dataSets: Array<number>, labelName: string){
    this.chartDataset.push({
      data: dataSets,
      label: labelName
    });
  }

  assignColorToChart(colorArray: Array<any>, colorText: string){
    colorArray.push({
      backgroundColor: colorText,
      borderColor: 'black',
      pointBackgroundColor: colorText,
      pointBorderColor: colorText,
      pointHoverBackgroundColor: colorText,
      pointHoverBorderColor: colorText
    });
  }

}
