export class CombinedResponse {
  date: string;
  cases: number;
  deaths: number;
  recovered: number;

  constructor() {
  }
  /*
  constructor(_date: string, _cases: number, _deaths: number, _recovered: number) {
    this.date = _date;
    this.cases = _cases;
    this.deaths = _deaths;
    this.recovered = _recovered;
  }
  */
}
