export class StateResponse {
  stateName: string;
  stateCases: number;
  stateDeaths: number;
  stateRecovered: number;

  constructor(_stateName: string, _stateCases: number, _stateDeaths: number, _stateRecovered: number) {
    this.stateName = _stateName;
    this.stateCases = _stateCases;
    this.stateDeaths = _stateDeaths;
    this.stateRecovered = _stateRecovered;
  }
}
