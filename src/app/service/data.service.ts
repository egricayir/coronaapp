import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject, forkJoin} from 'rxjs';
import {map} from 'rxjs/operators';
import {StateResponse} from '../models/stateResponse';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private BASE_GERMANY_URL = environment.BASE_GERMANY_URL;
  private BASE_STATES_URL = environment.BASE_STATES_URL;

  // stateCodeSubject = new BehaviorSubject<string>("");
  // stateResponseSubject = new BehaviorSubject<StateResponse>(new StateResponse("", 0, 0, 0));

  constructor(private http: HttpClient) { }

  getCurrentGermany(){
    const getCurrentGermanyURL = this.BASE_GERMANY_URL;

    return this.http.get(getCurrentGermanyURL);
  }

  getCasesGermanyWithDays(days: number){
    const getCasesGermanyWithDaysURL = this.BASE_GERMANY_URL + 'history/cases/' + days;

    return this.http.get(getCasesGermanyWithDaysURL);
  }

  getDeathsGermanyWithDays(days: number){
    const getDeathsGermanyWithDaysURL = this.BASE_GERMANY_URL + 'history/deaths/' + days;

    return this.http.get(getDeathsGermanyWithDaysURL);
  }

  getRecoveredGermanyWithDays(days: number){
    const getRecoveredGermanyWithDaysURL = this.BASE_GERMANY_URL + 'history/recovered/' + days;

    return this.http.get(getRecoveredGermanyWithDaysURL);
  }

  getCombinedGermany(state: string, days: number){
    let casesGermany = this.getCasesGermanyWithDays(days).pipe(map(
      result => {
        return result["data"];
      }
    ));
    let deathsGermany = this.getDeathsGermanyWithDays(days).pipe(map(
      result => {
        return result["data"];
      }
    ));
    let recoveredGermany = this.getRecoveredGermanyWithDays(days).pipe(map(
      result => {
        return result["data"];
      }
    ));

    return forkJoin([casesGermany, deathsGermany, recoveredGermany]);
  }

  getAllStates(){
    const getAllStatesURL = this.BASE_STATES_URL;

    return this.http.get(getAllStatesURL);
  }

  getCurrentFromState(state: string){
    const getCurrentFromStateURL = this.BASE_STATES_URL + state;

    return this.http.get(getCurrentFromStateURL);
  }

  getCasesFromStateWithDays(state: string, days: number){
    const getCasesFromStateWithDaysURL = this.BASE_STATES_URL + state + '/history/cases/' + days;

    return this.http.get(getCasesFromStateWithDaysURL);
  }

  getDeathsFromStateWithDays(state: string, days: number){
    const getDeathsFromStateWithDaysURL = this.BASE_STATES_URL + state + '/history/deaths/' + days;

    return this.http.get(getDeathsFromStateWithDaysURL);
  }

  getRecoveredFromStateWithDays(state: string, days: number){
    const getRecoveredFromStateWithDaysURL = this.BASE_STATES_URL + state + '/history/recovered/' + days;

    return this.http.get(getRecoveredFromStateWithDaysURL);
  }

  getCombinedState(state: string, days: number){
    let casesState = this.getCasesFromStateWithDays(state, days).pipe(map(
      result => {
        return result["data"][state]["history"];
      }
    ));
    let deathsState = this.getDeathsFromStateWithDays(state, days).pipe(map(
      result => {
        return result["data"][state]["history"];
      }
    ));
    let recoveredState = this.getRecoveredFromStateWithDays(state, days).pipe(map(
      result => {
        return result["data"][state]["history"];
      }
    ));

    return forkJoin([casesState, deathsState, recoveredState]);
  }
}
